<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
-->
<iso:schema
        xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:iso="http://purl.oclc.org/dsdl/schematron"
        queryBinding='xslt2'
        schemaVersion='ISO19757-3'>

    <iso:title>Validation for GetCareDocumentation</iso:title>
    <iso:ns prefix='urn' uri='urn:riv:clinicalprocess:healthcond:description:GetCareDocumentationResponder:3' />
    <iso:ns prefix='urn1' uri='urn:riv:clinicalprocess:healthcond:description:3' />

	<iso:ns prefix='soapenv' uri='http://schemas.xmlsoap.org/soap/envelope/'/>

	<!-- Rules for abstract patterns -->
	<iso:pattern abstract="true" id="pattern.CvType">
		<iso:rule context="$path">
			<iso:assert test="count(urn1:code) + count(urn1:originalText) >= 1">
				In $path, code and/or originalText must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:code">
			<iso:assert test="count(../urn1:codeSystem) = 1">
				In $path if <iso:name/> is given, codeSystem must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:codeSystem">
			<iso:assert test="count(../urn1:code) = 1">
				In $path if <iso:name/> is given, code must be given.</iso:assert>
			<iso:assert test="matches(.,'^[0-2](\.([0-9])+)+$')">
                Element 'codeSystem' must be an OID for the current codeSystem.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:codeSystemName">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) = 2">
				In $path if <iso:name/> is given, code and codeSystem must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:codeSystemVersion">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) = 2">
				In $path if <iso:name/> is given, code and codeSystem must be given.</iso:assert>
		</iso:rule>
		<iso:rule context="$path/urn1:displayName">
			<iso:assert test="count(../urn1:code) + count(../urn1:codeSystem) = 2">
				In $path if <iso:name/> is given, code and codeSystem must be given.</iso:assert>
		</iso:rule>
	</iso:pattern>
	
	<!-- Elements that implements abstract patterns -->
	<iso:pattern id="byRole" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:byRole" />
	</iso:pattern>
	<iso:pattern id="clinicalDocumentNoteCode" is-a="pattern.CvType">
		<iso:param name="path" value="urn1:clinicalDocumentNoteCode" />
	</iso:pattern>
	
	<!--                            -->
	<!-- Rules for various elements -->
	<!--                            -->
	
    <iso:pattern id="careDocumentationBody">
        <iso:rule context="urn:careDocumentation">
            <iso:assert test="count(urn1:body/urn1:clinicalDocumentNoteText) + count(urn1:body/urn1:multimediaEntry) = 1">
                Element body: One of clinicalDocumentNoteText or multimediaEntry must be given, but not both.</iso:assert>
        </iso:rule>
    </iso:pattern>

    <iso:pattern id="multimediaEntry">
        <iso:rule context="urn1:multimediaEntry">
			<iso:assert test="count(urn1:value) + count(urn1:reference) = 1">Element multimediaEntry: one of 'value' or 'reference' must be given, but not both.</iso:assert>
        </iso:rule>
    </iso:pattern>
	
	<iso:pattern id="Verify non-empty elements">
        <iso:rule context="soapenv:Body/urn:GetCareDocumentationResponse//*">
		    <iso:assert test="normalize-space(.)">Element <iso:name /> is included but empty. All elements included in the response must have valid values.</iso:assert>
        </iso:rule>
    </iso:pattern>
	
	<iso:pattern id="Verify non existing extension">
        <iso:rule context="urn:careDocumentation/urn1:header/urn1:record/urn1:id">
            <iso:assert test="count(urn1:extension) = 0">
                Element record/id/extension must NOT be given.</iso:assert>
        </iso:rule>
	</iso:pattern>
	<iso:pattern id="Verify unique recordId">
		<iso:rule context="urn:GetCareDocumentationResponse">
			<let name="recordIdPath" value="urn:careDocumentation/urn1:header/urn1:record/urn1:id/urn1:root"/>
			<iso:assert test="count($recordIdPath) = count(distinct-values($recordIdPath))">Element 'record/id/root' must contain unique id's</iso:assert>
		</iso:rule>
	</iso:pattern>
	
</iso:schema>
